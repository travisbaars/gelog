use clap::{Parser, Subcommand, ValueEnum};

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
#[command(propagate_version = true)]
pub struct Cli {
    #[command(subcommand)]
    pub commands: Commands,
}

#[derive(Subcommand)]
pub enum Commands {
    /// List previous transactions
    List {
        #[clap(short, long)]
        /// Set the history depth
        depth: Option<u32>,
    },

    /// Create a new transaction
    New {
        order: OrderType,

        /// Item name
        item: String,

        /// Price per item
        price: i32,

        /// Quantity traded
        quantity: i32,

        #[clap(short, long, default_value_t = true)]
        /// Send transaction to printer [default = true]
        print: bool,
    },
    /// Manage application settings
    Setup { setup: bool },
}

#[derive(Clone, Copy, ValueEnum)]
pub enum Setup {
    Printer,
    Database,
}

#[derive(Clone, Copy, ValueEnum)]
pub enum OrderType {
    Buy,
    Sell,
}
